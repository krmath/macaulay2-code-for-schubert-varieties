## Name
Macaulay2 code for computations with Schubert varieties in the flag variety

## Description
This file contains code written for Macaulay2 to run some computations on Schubert varieties in the flag variety. Comments were written for clarity, but please feel free to open an issue on gitlab if there is any confusion or ambiguity. Where terminology is not consistent or well established, please refer to "Governing Singularities of Schubert Varieties" by Alexander Woo and Alexander Wong, which contains some code that inspired this.

## Installation
This is just code that can be run in M2, so there is nothing to install if you already have M2. If not, please see the official website at http://www2.macaulay2.com/Macaulay2/

## Usage
The comments for each function should explain usage clearly. Please open an issue on gitlab if any seem vague or ambiguous.

## Support
This is code that I wrote for my own research. I do not know if anyone will find it useful, but I did my best to make it easy to follow if this is the case. Please open an issue on gitlab if you run into any problems or have any suggestions.

## Roadmap
As this is something I wrote for my own research, the current plan is to update it if and only if my research demands it. If others find it useful and there is demand to add to it, these plans may change,

## Contributing
I would be happy to accept any functional additions to this project.

## Authors and acknowledgment
Thanks to Alexander Woo and Alexander Yong for writing some early versions of some of these functions.

## Project status
I suppose that I am actively maintaining this in that I intend to keep the version on gitlab up to date with my personal version, but do not expect much need for updates, if any.

