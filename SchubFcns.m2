----------------------------------------------------

perMat = (m) -> (
    -- Input: m, a permutation, written in one line
    -- notation as a list.
    -- Output: The matrix representation of m.
   n := #m;
   Rows := for i from 0 to n-1 list(
       L1 = for j from 0 to n-1 list(
	   if i+1 == m#j then 1 else 0
	   );
       matrix{L1}
       );
   A := Rows#0;
   for i from 1 to n-1 do (A = A||Rows#i);
   A
)

-----------------------------------------------------

rkMat = (m) -> (
    -- Input: m, a permutation, written in one line
    -- notation as a list.
    -- Output: The rank matrix associated to m.
   n := #m;
   Rows := for i from 0 to n-1 list(
       L1 = for j from 0 to n-1 list(
	       if i+1 == m#j then 1 else 0
	   );
       matrix{L1}
       );
   A := Rows#0;
   for i from 1 to n-1 do(
       A = A||Rows#i
       );
   matrix table(n,n, (i,j) -> (
	   rank A_{0..j}^{i..(n-1)}
	   )
       )
)


---------------------------------------------------


opMa = (m) -> (
	-- Input: m, a permutation, written in one line notation as a list.
	-- Output: The generic matrix in the opposite Schubert cell of m with 0s in place of ts.
	n := #m;
	R := QQ[z_(1,1)..z_(n,n)];
	A := matrix table (n,n, (i,j) -> (
		if i+1 == m#j then 1 else 0)
		);
	matrix table(n,n, (i,j) -> (
			if (
				L1 = for k from 0 to j list A_(i,k);
				L2 = for k from i to n-1 list A_(k,j);
				sum(L1) + sum(L2) == 0
				)
			then z_(n-i,j+1)
				-- The indexing here makes the bottom left entry correspond
				-- to z_(1,1) and the top right to z_(n,n). Remember M2
				-- counts from 0.
			else (
					0
					)
				)
			)
)

------------------------------------------------------

opMat = (m,R) -> (
	-- Input: m, a permutation, written in one line notation as a list
    --        R, a base ring
	-- Output: The generic matrix in the opposite Schubert cell of m.
	n := #m;
	use R;
	A := matrix table (n,n, (i,j) -> (
		if i+1 == m#j then 1 else 0)
		);
	matrix table(n,n, (i,j) -> (
			if (
				L1 = for k from 0 to j list A_(i,k);
				L2 = for k from i to n-1 list A_(k,j);
				sum(L1) + sum(L2) == 0
				)
			then z_(n-i,j+1)
				-- The indexing here makes the bottom left entry correspond
				-- to z_(1,1) and the top right to z_(n,n). Remember M2
				-- counts from 0.
			else (
					if (A_(i,j) == 1) then t else 0
					)
				)
			)
)



------------------------------------------------------

SVar = (m) -> (
        -- Input: m, a permutation, written in one line notation as a list
	-- Output: The variables for the polynomial ring S, written as a list
	
	L := flatten entries opMa(m);
        for i from 0 to #L-1 list (if (L#i == 0) then continue else L#i)
)

----------------------------------------------------------

SVari = (m) -> (
        -- Input: m, a permutation, written in one line notation as a list
	-- Output: The variables for the polynomial ring S, written as a list with 1 first
	
	insert(0, 1, SVar(m))
)

----------------------------------------------------------

StVar = (m) -> (
    	-- Input: m, a permutation, written in one line notation as a list
	-- Output: the variables for the polynomial ring St, written as a list with t first
	
	insert(0, t, SVar(m))
)

---------------------------------------------------

mult = (u,m) -> (
    -- Input: n, m, permutations, written in one line
    --   notation as a list where n is for a point and m is for a variety
    -- Output: the multiplicity of X_m at id
    St := QQ[toSequence StVar(u), MonomialOrder=>Eliminate 1]; n:=#m;p:=#SVar(u);
    genMat := opMat(u,St); -- Generic matrix in opposite
	                                        -- Schubert cell of u
    Rank := rkMat(m);
    Jlist := trim(sum(flatten(for i from 0 to n-1 list
		               for j from 0 to n-1 list
			         minors(Rank_(n-i-1,j)+1,
				 submatrix(genMat, {(n-i-1)..n-1}, {0..j})))));
    GBlist := gb(Jlist);
    LTlist := leadTerm(gens(GBlist));
    S := QQ[toSequence drop (gens St, {0,0})];
    f := map(S, St, insert(0,1, gens(S)));
    ELTlist := f(LTlist);
    Dlist := degree(ideal(ELTlist))
    )

----------------------------------------------------

meet = (u,v) -> (
    -- Input: m,n, permutations, written in one line notation
    -- Output: The meet of the rank matrices for m,n
    A := rkMat(u);
    B := rkMat(v);
    n := #u;
    matrix table (n,n, (i,j) -> (min {A_(i,j),B_(i,j)})
	)
)

-------------------------------------------------------

lgth = (u) -> (
    -- Input: u, a permutation, written in one line notation
    -- The Coxeter length of u
    n := #u;
    LL := for i from 0 to n-1 list(
	L1 = for j from i+1 to n-1 list(
	    if u#i > u#j then 1 else 0
	    );
	L1
	);
    sum(flatten(LL))
)

-------------------------------------------------------

reduc = (u) -> (
    -- Input: u: list of distinct positive integers
    -- Output: permutation in one-line notation on 0..#u in same relative order as u
    n := #u;
    b := u;
    L := {};
    for i from 0 to n-1 do (
	L = append(L, minPosition(b));
	t_i = minPosition(b);
	b = drop(b,{t_i,t_i});
	b = insert(t_i,max(b)+1,b)
	);
    for i from 0 to n-1 list(position(L,k -> k == i)+1)
)

------------------------------------------------------

embed = (u,v) -> (
    --Input: u,v: permutations in one-line notation with #u < #v
    --Output: 1 if u embeds in v, 0 if v avoids u
    n := #u;
    L := subsets(v,n);
    K := for i from 0 to #L-1 list(if reduc(L#i) == u then 1 else 0);
    M := sum(K);
    if M == 0 then print 0 else print 1;
)